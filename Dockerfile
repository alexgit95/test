FROM maven:3.6.1-jdk-11-slim as builder
WORKDIR /apps
COPY . /apps/


RUN mvn package -DskipTests=true


FROM openjdk:11-jre-slim
#pas de distroless dispo pour raspberry
WORKDIR /apps
COPY --from=builder /apps/target/test-0.0.1-SNAPSHOT.jar /apps/
CMD java -jar /apps/test-0.0.1-SNAPSHOT.jar
