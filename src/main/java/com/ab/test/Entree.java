package com.ab.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@RequestMapping("/api")
public class Entree {
	@RequestMapping( value="/hello", method = RequestMethod.GET)
	public @ResponseBody String getAllValidated(){
		return "coucou";
	}

}
